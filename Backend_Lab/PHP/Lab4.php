<?php
$host = 'localhost';
$username = 'root';
$password = '';
$dbname = 'cse455';
$dsn = 'mysql:host='. $host .';dbname='. $dbname;
$name = $_GET['Euro'];
$rate = $_GET['rate'];

try {
  $pdo = new PDO($dsn, $username, $password);
  $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE);
  $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
  $sql = 'SELECT * FROM currency WHERE currency_name=?';
  $stmt = $pdo->prepare($sql);
  $stmt->execute([$name]);
  $posts = $stmt->fetch(PDO::FETCH_OBJ);
  $conObj->conversion = $posts->currency_rate * $rate;
  echo json_encode($conObj);
}

catch(PDOException $e) {
  echo "Connection failed: ". $e->getMessage();
}
?>